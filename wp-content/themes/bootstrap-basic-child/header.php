<?php
/**
 * The theme header
 * 
 * @package bootstrap-basic
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<!--wordpress head-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="page-container">
		<?php // do_action('before'); ?> 
		<?php // if( !is_front_page( ) ): ?>
		<header role="banner" class="top-header">
			<div class="container-fluid">
			 <div class="row site-branding">
				<div class="col-md-6 site-title">
					<h1 class="site-title-heading">
						<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>
					</h1>
					<div class="site-description">
						<small>
							<?php bloginfo('description'); ?> 
						</small>
					</div>
				</div>
				<div class="col-md-6 page-header-top-right">
					<div class="sr-only">
						<a href="#content" title="<?php esc_attr_e('Skip to content', 'bootstrap-basic'); ?>"><?php _e('Skip to content', 'bootstrap-basic'); ?></a>
					</div>
					<?php if (is_active_sidebar('header-right')) { ?> 
					<div class="pull-right">
						<?php dynamic_sidebar('header-right'); ?> 
					</div>
					<div class="clearfix"></div>
					<?php } // endif; ?> 
				</div>
			</div><!--.site-branding-->
			
			<div class="row main-navigation">
				<div class="col-lg-1 col-md-2 col-sm-1 col-xs-4">
					<div class="logo">
						<figure>
							<a href="http://beta.hestawork.com/wraynephew/">
								<img src="<?php echo get_stylesheet_directory_uri().'/assets/img/logo-1.png';?>" alt="logo">
							</a>
						</figure>
					</div>
				</div>
				<div class="col-lg-9 col-md-8 col-sm-9 col-xs-8">
					<nav class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-primary-collapse">
								<span class="sr-only"><?php _e('Toggle navigation', 'bootstrap-basic'); ?></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						
						<div class="collapse navbar-collapse navbar-primary-collapse">
							<?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapBasicMyWalkerNavMenu())); ?> 
							<?php dynamic_sidebar('navbar-right'); ?> 
							<div class="header-social vissible-xs hidden-sm hidden-md hidden-lg">
								<ul>
									<li>
										<a href="#/">
											<i class="fa fa-instagram"></i>
										</a>
									</li>
									<li>
										<a href="#/">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li>
										<a href="#/">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
								</ul>
							</div>
						</div><!--.navbar-collapse-->
					</nav>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
					<div class="header-social">
						<ul>
							<li>
								<a href="#/">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<li>
								<a href="#/">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#/">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div><!--.main-navigation-->
			</div>
		</header>
		<?php// endif; ?>
		
		<div id="content" class="row row-with-vspace site-content">
