<?php
function filter_search($query) {
    if ($query->is_search) {
		$query->set('post_type', array('post', 'tribe_events','give_forms'));
    };
    return $query;
}

add_filter('pre_get_posts', 'filter_search');

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
function d( $data ) {
	echo '<pre>', print_r($data);
} 
add_action( 'init', 'codex_book_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_book_init() {
	$labels = array(
		'name'               => _x( 'Newsletter', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'All Newsletters', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Newsletter', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Newsletter', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Newsletter', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Newsletter', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Newsletter', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Newsletter', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Newsletter', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Newsletters', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Newsletter', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Newsletter:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Newsletter found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Newsletter found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'age_verify' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title')
	);

	register_post_type( 'age_verify', $args );
}
//Register Meta Box
function rm_register_meta_box() {
    add_meta_box( 'rm-meta-box-id', esc_html__( 'User details', 'text-domain' ), 'rm_meta_box_callback', 'age_verify', 'advanced', 'high' );
}
add_action( 'add_meta_boxes', 'rm_register_meta_box');
 
function rm_meta_box_callback( $meta_id ) {
	global $post;
	$name = get_post_meta($post->ID, 'av_name', true);
	$email = get_post_meta($post->ID, 'email_id', true);
	?>
		<div class="news-letter-list">
			<div class="newsletter-name"> Name: <span><?php echo ( !empty($name) ) ? $name : '-' ; ?></span> </div>
			<div class="newsletter-email"> Email: <span><?php echo ( !empty($email) ) ? $email : '-' ; ?></span> </div>
			<div class="newsletter-age"> Age: <span><?php echo get_post_meta($post->ID, 'age', true); ?></span> </div>
		</div>
	<?php 
}
add_action('av_was_verified', function( ) {
	$post_arr = array(
	    'post_title'   => '#' ,
	    'post_type'	=>	'age_verify',
	    'meta_input'   => array(
	        'email_id' => sanitize_text_field( $_REQUEST['EMAIL'] ),
	        'av_name' => sanitize_text_field( $_REQUEST['av_name'] ),
	        'age'	=>	trim( $_REQUEST['av_verify_d'].'-'.$_REQUEST['av_verify_m'].'-'.	$_REQUEST['av_verify_y'] ),
	    ),
	);
	$post_id =  wp_insert_post( $post_arr ) ;
	wp_update_post( [ 'ID' => $post_id, 'post_title' => '#'.$post_id  ] );
	// echo do_shortcode('[mc4wp_form id="466"]');
	// die;
  	// try {
  	// 	new MC4WP_Custom_Integration( );
  	// } catch (Exception $e) {
			// die( $e );
  	// }
  	// die;

});