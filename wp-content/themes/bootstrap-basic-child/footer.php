<?php
/**
 * The theme footer
 * 
 * @package bootstrap-basic
 */
?>

			</div><!--.site-content-->
			
			<?php /*
			
			*/ ?>
		</div><!--.container page-container-->	
		
		<!--wordpress footer-->
		<?php wp_footer(); ?> 
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('.navbar-default .navbar-toggle').click(function(){
					jQuery('header.top-header .navbar-collapse').slideToggle('slow');
				});
				jQuery('a.read-more-btn').click(function(){
					 jQuery(this).text(function(i, v){
		               return v === 'Read less' ? 'Read more...' : 'Read less'
		            })
					jQuery('span.toggle-content').slideToggle('slow');
				});
			});
			jQuery(window).load(function(){
				jQuery('.nf-form-content input.pikaday__display.datepicker').attr('placeholder', 'DD/MM/YY');
			});	
		</script>
	</body>
</html>